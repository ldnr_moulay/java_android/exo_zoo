package fr.ldnr.formation.zoo;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AlerteActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alerte);
        // relier tableau de lieu, composant avec auto-complétion et agencement d'un élément
        AutoCompleteTextView etLieu = findViewById(R.id.etLieu);
        ZooDbHelper dbHelper = new ZooDbHelper(this);
        String[] lieux = dbHelper.getLieux();
        ArrayAdapter<String> aa = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, lieux);
        etLieu.setAdapter(aa);
    }

    public void clicEnvoi(View view) {
        Log.i("AlerteActivity", "click !");

        ZooDbHelper dbHelper = new ZooDbHelper(this);

        EditText edLieu = findViewById(R.id.etLieu);
        String lieu = edLieu.getText().toString();
        dbHelper.enregistrerLieu(lieu);

        String message = dbHelper.getNombreParLieu(lieu) +
                " alerte(s) pour ce lieu";
        TextView tvMessage = findViewById(R.id.tvMessage);
        tvMessage.setText(message);

        if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            ecrireFichier();
        } else {
            requestPermissions(new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, 0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            ecrireFichier();
        }
    }

    public void ecrireFichier() {
        Log.i("AlerteActivity", "Ecriture du fichier");
        if(Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            File repertoire = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS);
            // repertoire.mkdirs();
            File fichierLog = new File(repertoire, "alertes.txt");
            Log.i("AlerteActivity", "Fichier : "+fichierLog);
            try {
                FileWriter fw = new FileWriter(fichierLog, true);
                EditText titre = findViewById(R.id.etTitre);
                EditText lieu = findViewById(R.id.etLieu);
                EditText description = findViewById(R.id.etDescription);
                CheckBox urgent = findViewById(R.id.cbUrgent);
                String texteUrgent = "non";
                if(urgent.isChecked())
                    texteUrgent = "oui";
                fw.write(titre.getText()+", "+lieu.getText()+", "+
                        description.getText()+", "+urgent+"\n");
                fw.close();
            } catch(IOException ex) {
                Log.e("AlerteActivity", "Erreur IO : ", ex);
            }
        }
    }
}
