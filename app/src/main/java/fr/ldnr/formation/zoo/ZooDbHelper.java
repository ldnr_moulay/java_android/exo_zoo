package fr.ldnr.formation.zoo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

public class ZooDbHelper extends SQLiteOpenHelper {

    public ZooDbHelper(@Nullable Context context) {
        super(context, "zoo.sqlite", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // creation des tables
        sqLiteDatabase.execSQL(
                "CREATE TABLE lieux(id INTEGER PRIMARY KEY, nom TEXT)"
        );
        sqLiteDatabase.execSQL("INSERT INTO lieux(nom) VALUES(?)", new Object[]{
                "Enclos des singes"
        });
        sqLiteDatabase.execSQL("INSERT INTO lieux(nom) VALUES(?)", new Object[]{
                "Chemin central"
        });
        sqLiteDatabase.execSQL("INSERT INTO lieux(nom) VALUES(?)", new Object[]{
                "Enclos des varans"
        });
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase,
                          int ancienneVersion, int nouvelleVersion) {
        // mise à jour des tables
    }

    public void enregistrerLieu(String nom) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("INSERT INTO lieux(nom) VALUES(?)", new Object[]{ nom });
        db.close();
    }

    public int getNombreParLieu(String nom) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(
                "SELECT COUNT(*) FROM lieux WHERE nom LIKE ?",
                new String[]{ nom });
        c.moveToNext();
        int res = c.getInt(0);
        c.close();
        db.close();
        return res;
    }

    public String[] getLieux() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT DISTINCT(nom) FROM lieux ORDER BY nom", null);
        ArrayList<String> l = new ArrayList<>();
        while(c.moveToNext())
            l.add(c.getString(0));
        c.close();
        db.close();
        return l.toArray(new String[0]);
    }
}
