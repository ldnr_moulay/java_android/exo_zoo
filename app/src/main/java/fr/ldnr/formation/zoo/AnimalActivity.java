package fr.ldnr.formation.zoo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AnimalActivity extends Activity {

    // un handler sert a guider les threads
    private Handler uiHandler;
    private String extract;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // c'est par le biais de cela qu'ont pourra appeler les methodes dans le xml
        setContentView(R.layout.animal);
    }

    /*public void trouver(View view){
    Log.i("AnimalActivity", "trouver");
    Thread t = new Thread(new Runnable() {
        @Override
        public void run(){
            rechercher();
        }
    });
    t.start();
}*/
    public void trouver(View view) {
        Log.i("AnimalActivity", "trouver");
        //cree un thread qui va faire executer la tache liee chacun son tour
        //ExecutorService executor = Executors.newSingleThreadExecutor();
        //cree le nombre de thread demande en parametre du threadpool pour executer les tache liees en simultanee
         ExecutorService executor = Executors.newFixedThreadPool(2);
         // appel du handler dans la boucle principale
         uiHandler = new Handler(Looper.getMainLooper());

        Runnable r = new Runnable() {
            @Override
            public void run() {
                rechercher();
            }
        };
        // a cause du newsinglethreadexecutor les taches vont etre execute chacun leur tour
        // en utilisant le fixedthreadpool les taches vont etre executees en parallele
        executor.execute(r);
        executor.execute(r);
        executor.execute(r);
    }

    public void rechercher() {
        EditText etEspece = findViewById(R.id.etEspece);
        EditText etInformations = findViewById(R.id.etInformations);
        try {
        String url = "https://fr.wikipedia.org/w/api.php?action=query&prop=extracts&exsentences=2&format=json&titles="+
                URLEncoder.encode(etEspece.getText().toString(), "UTF-8");
                etEspece.getText().toString();
            //connexion a internet
            URLConnection conn = new URL(url).openConnection();
            InputStream is = conn.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String resultat = "";
            String ligne;
            while ((ligne = br.readLine()) != null)
                resultat += ligne + "\n";
            Log.i("AnimalActivity", "Recu debut : "+ resultat); //.substring(0,100)
            is.close();
            Log.i("AnimalActivity", "Connexion fermée");
            JSONObject base = new JSONObject(resultat);
            JSONObject query = base.getJSONObject("query");
            JSONObject pages = query.getJSONObject("pages");
            String numeroPage = pages.keys().next();//numero de la premiere page renvoyee
            JSONObject page = pages.getJSONObject(numeroPage);
            extract = page.getString("extract");
            //enlever les balises html
            extract = Html.fromHtml(extract).toString();
            Log.i("AnimalActivity" ,"recu html :"+ extract);
            //demande d'execution de la tache dans le thread principal
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    //ecriture de extract dans la case information
                    etInformations.setText(extract);

                }
            });

        } catch (Exception e) {
            Log.e("AnimalActivity", "Erreur", e);
        }
    }

    public void enregistrer(View view) {
        Log.i("AnimalActivity", "enregistrer");
    }

}
