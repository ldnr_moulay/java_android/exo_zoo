package fr.ldnr.formation.zoo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class PopcornActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new PopcornView(this));
        Log.i("PopcornActivity", "Fin de onCreate()");
        int tempsPasse = (int)(getIntent().getLongExtra("temps", 0) / 1000);
        if(tempsPasse>2) {
            Toast.makeText(this,
                getResources().getQuantityString (
                        R.plurals.popcorn_avertissement,
                        tempsPasse, tempsPasse),
                Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getActionMasked()==MotionEvent.ACTION_DOWN) {
            if(event.getY() < 500) { // en haut
                Intent i = new Intent(this, CarteActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            } else { // en bas
                String strUrl = "https://tinyurl.com/th6h7yn9";
                try {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(strUrl));
                    startActivity(i);
                } catch(Exception ex) {
                    Log.e("PopcornActivity", "Pas de navigateur !", ex);
                }
            }
        }
        return true;
    }

    class PopcornView extends View {

        public PopcornView(Context context) {
            super(context);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            Bitmap bmp = BitmapFactory.decodeResource(
                    getResources(), R.drawable.popcorn);
            canvas.drawBitmap(bmp, 0, 0, null);
        }
    }
}
