package fr.ldnr.formation.zoo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class AquariumActivity extends Activity {

    long debut, fin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new AquariumView(this));
        Log.i("AquariumActivity", "Fin de onCreate()");
        debut = System.currentTimeMillis();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getActionMasked()==MotionEvent.ACTION_DOWN) {
            Intent i = new Intent(this, PopcornActivity.class);
            fin = System.currentTimeMillis();
            long tempsPasse = fin - debut;
            i.putExtra("temps", tempsPasse);
            startActivity(i);
        }
        return true;
    }

    class AquariumView extends View {

        public AquariumView(Context context) {
            super(context);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            Bitmap bmp = BitmapFactory.decodeResource(
                    getResources(), R.drawable.aquarium);
            canvas.drawBitmap(bmp, 0, 0, null);
        }
    }
}
