package fr.ldnr.formation.zoo;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AccueilActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accueil);
        try {
            InputStream is = getAssets().open("news.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String nouvelles = "";
            String ligne;
            while ((ligne = br.readLine()) != null)
                nouvelles += ligne + "\n";

            is.close();

            TextView tvNouvelles = findViewById(R.id.tvNouvelles);
            tvNouvelles.setText(nouvelles);
        } catch (IOException ex) {
            Log.e("AccueilActivity", "Erreur de lecture", ex);
        }

        Button btCarte = findViewById(R.id.btCarte);
        btCarte.setOnClickListener(this);

        Button btAlerte = findViewById(R.id.btAlerte);
        btAlerte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AccueilActivity.this, AlerteActivity.class);
                startActivity(i);
            }
        });

        Button btAnnuaire = findViewById(R.id.btAnnuaire);
        btAnnuaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AccueilActivity.this, AnnuaireActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btCarte) { // inutile
            Intent i = new Intent(this, CarteActivity.class);
            startActivity(i);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.accueil, menu);
        // lecture dans les préférences
        SharedPreferences sp = getSharedPreferences("zoo", MODE_PRIVATE);
            boolean enregistrement = sp.getBoolean("enregistrement", true);
            menu.findItem(R.id.menu_enregistrer).setChecked(enregistrement);
            return true;
        }

        @Override
        public boolean onMenuItemSelected(int featureId, @NonNull MenuItem item) {
            switch(item.getItemId()) {
                case R.id.menu_carte:
                    startActivity(new Intent(this, CarteActivity.class));
                    return true;
                case R.id.menu_animal:
                    startActivity(new Intent(this, AnimalActivity.class));
                    return true;
                case R.id.menu_enregistrer:
                    item.setChecked(!item.isChecked());
            // enregistrement dans les preferences
            SharedPreferences sp = getSharedPreferences("zoo", MODE_PRIVATE);
            SharedPreferences.Editor ed = sp.edit();
            ed.putBoolean("enregistrement", item.isChecked() );
            ed.commit();
            return true;
        default:
            return super.onMenuItemSelected(featureId, item);
        }
    }
}

